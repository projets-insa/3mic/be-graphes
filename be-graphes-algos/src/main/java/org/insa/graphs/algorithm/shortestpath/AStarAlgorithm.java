package org.insa.graphs.algorithm.shortestpath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.insa.graphs.algorithm.AbstractInputData.Mode;
import org.insa.graphs.model.Node;

public class AStarAlgorithm extends DijkstraAlgorithm {

    public AStarAlgorithm(ShortestPathData data) {
        super(data);
    }
    
    @Override
    protected ArrayList<Label> createLabels(List<Node> nodes) {
    	Node destination = this.getInputData().getDestination();
    	ArrayList<Label> labels = new ArrayList<Label>();
    	for(Node node: nodes) {
    		LabelStar label = new LabelStar(node);
    		if(this.data.getMode() == Mode.LENGTH) {
    			label.setEstimatedDistanceCost(destination);
    		} else {
    			label.setEstimatedTimeCost(destination, this.data.getMaximumSpeed());
    		}
    		labels.add(label);
    	}
    	return labels;
    }
}
