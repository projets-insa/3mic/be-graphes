package org.insa.graphs.algorithm.shortestpath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.insa.graphs.algorithm.AbstractInputData.Mode;
import org.insa.graphs.algorithm.AbstractSolution;
import org.insa.graphs.algorithm.utils.BinaryHeap;
import org.insa.graphs.algorithm.utils.ElementNotFoundException;
import org.insa.graphs.model.Arc;
import org.insa.graphs.model.Node;
import org.insa.graphs.model.Path;

public class DijkstraAlgorithm extends ShortestPathAlgorithm {

    public DijkstraAlgorithm(ShortestPathData data) {
        super(data);
    }
    
    protected ArrayList<Label> createLabels(List<Node> nodes) {
    	ArrayList<Label> labels = new ArrayList<Label>();
    	for(Node node: nodes) {
    		labels.add(new Label(node));
    	}
    	return labels;
    }
    
    @Override
    protected ShortestPathSolution doRun() {
        final ShortestPathData data = getInputData();
        ShortestPathSolution solution = null;
        
        // the cost heap
        BinaryHeap<Label> heap = new BinaryHeap<Label>();
        
        // the labels
        ArrayList<Label> labels = createLabels(data.getGraph().getNodes());
        
        // Set the cost of the first label to 0
        Label originLabel = labels.get(data.getOrigin().getId());
        originLabel.setCost(0);
        
        // Insert the origin label into the cost heap 
        heap.insert(originLabel);
        
        notifyOriginProcessed(data.getOrigin());
        
    	Label destinationLabel = labels.get(data.getDestination().getId());
        while(!destinationLabel.hasBeenVisited() && !heap.isEmpty()) {
        	Label stepOrigin = heap.deleteMin();
        	stepOrigin.setHasBeenVisited(true);
        	// Check if the heap is still valid
        	assert(heap.isValid());
        	
        	notifyNodeMarked(stepOrigin.getNode());
        	
        	// Loop through the starting node successors
        	List<Arc> successors = stepOrigin.getNode().getSuccessors();
        	for(Arc successorArc : successors) {
        		Label successorLabel = labels.get(successorArc.getDestination().getId());
        		if(data.isAllowed(successorArc)) {
	        		boolean inHeap = successorLabel.getCost() != Double.MAX_VALUE;
	        		
	        		if(!inHeap) {
	        			notifyNodeReached(successorLabel.getNode());
	        		}
	        		
	        		// We only deal with the successor if it hasn't been visited yet
	        		if(!successorLabel.hasBeenVisited()) {	
	
		        		// Update the successor's cost and previous arc only if it is lower that the current
		        		double cost = stepOrigin.getCost() + data.getCost(successorArc);
		        		
		        		if(successorLabel.getCost() > cost) {
			        		// We update the successor in the heap
			        		// If the remove throws an exception, then the label isn't in the heap
			        		try {
			        			heap.remove(successorLabel);
			        		} catch (ElementNotFoundException e) {}
			        		
			        		successorLabel.setCost(cost);
			        		successorLabel.setPreviousArc(successorArc);
			        		successorLabel.setPreviousLabel(stepOrigin);
			        		
			        		heap.insert(successorLabel);
		        		}
	        		}
        		}
        	}
        }
        
        // If the destination's arc has not been set, then the path is infeasible
        if(!destinationLabel.hasPreviousArc()) {
        	solution = new ShortestPathSolution(data, AbstractSolution.Status.INFEASIBLE);
        } else {
        	notifyDestinationReached(data.getDestination());
        	
        	ArrayList<Arc> arcs = new ArrayList<Arc>();
        	Label label = destinationLabel;
        	while(label.hasPreviousArc()) {
        		arcs.add(label.getPreviousArc());
        		label = label.getPreviousLabel();
        	}
        	
        	Collections.reverse(arcs);
        	
        	Path path = new Path(data.getGraph(), arcs);
        	// Check if the path is valid
        	assert(path.isValid());
        	
        	solution = new ShortestPathSolution(data, AbstractSolution.Status.OPTIMAL, path);
        }
    
        return solution;
    }
}
