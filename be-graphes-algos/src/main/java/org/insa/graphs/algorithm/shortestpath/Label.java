package org.insa.graphs.algorithm.shortestpath;

import org.insa.graphs.model.Arc;
import org.insa.graphs.model.Node;

public class Label implements Comparable<Label> {
	protected Node node;
	
	protected boolean hasBeenVisited;
	
	protected double cost;
	
	protected Arc previousArc;
	
	protected Label previousLabel;
	
	public Label(Node node) {
		this.node = node;
		this.hasBeenVisited = false;
		this.cost = Double.MAX_VALUE;
		this.previousArc = null;
		this.previousLabel = null;
	}
	
	public void setPreviousArc(Arc arc) {
		this.previousArc = arc;
	}
	
	public void setPreviousLabel(Label label) {
		this.previousLabel = label;
	}
	
	public Label getPreviousLabel() {
		return this.previousLabel;
	}
	
	public double getCost() {
		return this.cost;
	}
	
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public void setHasBeenVisited(boolean b) {
		this.hasBeenVisited = b;
	}
	
	public boolean hasBeenVisited() {
		return this.hasBeenVisited;
	}
	
	public Node getNode() {
		return this.node;
	}
	
	public Arc getPreviousArc() {
		return this.previousArc;
	}
	
	public boolean hasPreviousArc() {
		return this.previousArc != null;
	}
	
	public double getTotalCost() {
		return this.cost;
	}

	@Override
	public int compareTo(Label o) {
		return Double.compare(this.getTotalCost(), o.getTotalCost());
	}
}
