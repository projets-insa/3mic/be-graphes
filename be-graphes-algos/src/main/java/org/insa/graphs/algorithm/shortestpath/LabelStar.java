package org.insa.graphs.algorithm.shortestpath;

import org.insa.graphs.model.Node;

public class LabelStar extends Label {
	private double estimatedCost;
	
	public LabelStar(Node node) {
		super(node);
	}
	
	public void setEstimatedDistanceCost(Node destination) {
		this.estimatedCost = this.node.getPoint().distanceTo(destination.getPoint());
	}
	
	public void setEstimatedTimeCost(Node destination, double speed) {
		this.estimatedCost = this.node.getPoint().distanceTo(destination.getPoint()) / speed;
	}
	
	public double getTotalCost() {
		return this.estimatedCost + this.cost;
	}
}
