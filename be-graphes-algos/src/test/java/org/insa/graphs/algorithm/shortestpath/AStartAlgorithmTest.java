package org.insa.graphs.algorithm.shortestpath;

public class AStartAlgorithmTest extends ShortestPathTest{
	@Override
	public ShortestPathSolution testShortestPath(ShortestPathData data) {
		return new AStarAlgorithm(data).doRun();
	}
}
