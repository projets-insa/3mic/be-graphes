package org.insa.graphs.algorithm.shortestpath;

public class DijkstraAlgorithmTest extends ShortestPathTest {
	@Override
	public ShortestPathSolution testShortestPath(ShortestPathData data) {
		return new DijkstraAlgorithm(data).doRun();
	}
}
