package org.insa.graphs.algorithm.shortestpath;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.insa.graphs.algorithm.ArcInspector;
import org.insa.graphs.algorithm.ArcInspectorFactory;
import org.insa.graphs.algorithm.shortestpath.ShortestPathData;
import org.insa.graphs.algorithm.shortestpath.ShortestPathSolution;
import org.insa.graphs.model.Graph;
import org.insa.graphs.model.io.BinaryGraphReader;
import org.insa.graphs.model.io.GraphReader;
import org.junit.Test;

public abstract class ShortestPathTest {
	public static final String CARRE_PATH = "../maps/carre.mapgr";
	public static final String INSA_PATH = "../maps/europe/france/insa.mapgr";
	
	protected static final ArcInspector[] arcInspectors = new ArcInspector[] {
		ArcInspectorFactory.getAllFilters().get(0),
		ArcInspectorFactory.getAllFilters().get(2)
	};
	
	// Represents a testing example
	protected class TestingData {
		public ShortestPathData data;
		public ShortestPathSolution solution;
		
		public TestingData(Graph graph, int originIndex, int destinationIndex, ArcInspector arcInspector) {
			this.data = new ShortestPathData(graph, graph.get(originIndex), graph.get(destinationIndex), arcInspector);
			this.solution = new BellmanFordAlgorithm(data).doRun();
		}
	}
	
	protected ArrayList<TestingData> testSuite;
	
	public ShortestPathTest() {
		this.generateTestSuite();
	}
	
	protected void generateTestSuite() {
		this.testSuite = new ArrayList<TestingData>();
		
		// Loading the map
		try {
			GraphReader reader = new BinaryGraphReader(new DataInputStream(new FileInputStream(ShortestPathTest.CARRE_PATH)));
			Graph carre = reader.read();
			
			reader = new BinaryGraphReader(new DataInputStream(new FileInputStream(ShortestPathTest.INSA_PATH)));
			Graph insa = reader.read();
			
			// Generating the Bellman-Ford oracles
			
			// Invalid path
			this.addTestingData(carre, 0, 0);
			this.addTestingData(insa, 0, 0);
			
			// Unfeasable path (one way)
			this.addTestingData(insa, 81, 80);
			this.addTestingData(insa, 1297, 1298);
			this.addTestingData(insa, 580, 334);
			
			// Valid path
			this.addTestingData(carre, 7, 10);
			this.addTestingData(insa, 125, 276);
			this.addTestingData(insa, 55, 710);
		} catch (IOException e ) {
			System.err.println("Missing file " + e.getMessage());
			System.exit(1);
		}
	}
	
	protected void addTestingData(Graph graph, int originIndex, int destinationIndex) {
		for(ArcInspector arcInspector : ShortestPathTest.arcInspectors) {
			this.testSuite.add(new TestingData(graph, originIndex, destinationIndex, arcInspector));
		}
	}
	
	@Test
	public void testSolutionStatus() {
		for(TestingData testingData : this.testSuite) {
			ShortestPathSolution testedSolution = testShortestPath(testingData.data);
			assert(testingData.solution.getStatus().equals(testedSolution.getStatus()));
		}
	}
	
	@Test
	public void testNoSolution() {
		for(TestingData testingData : this.testSuite) {
			ShortestPathSolution testedSolution = testShortestPath(testingData.data);
			assert(testingData.solution.getPath() != null || testingData.solution.getPath() == testedSolution.getPath());
		}
	}
	
	@Test
	public void testValidPath() {
		for(TestingData testingData : this.testSuite) {
			ShortestPathSolution testedSolution = testShortestPath(testingData.data);
			assert(testingData.solution.getPath() == null || testingData.solution.getPath().isValid() == testedSolution.getPath().isValid());
		}
	}
	
	@Test
	public void testSolutionLength() {
		for(TestingData testingData : this.testSuite) {
			ShortestPathSolution testedSolution = testShortestPath(testingData.data);
			boolean value = testingData.solution.getPath() == null || testingData.solution.getPath().getLength() == testedSolution.getPath().getLength();
			assert(value);
		}
	}
	
	@Test
	public void testSolutionTime() {
		for(TestingData testingData : this.testSuite) {
			ShortestPathSolution testedSolution = testShortestPath(testingData.data);
			assert(testingData.solution.getPath() == null || testingData.solution.getPath().getMinimumTravelTime() == testedSolution.getPath().getMinimumTravelTime());
		}
	}
	
	public abstract ShortestPathSolution testShortestPath(ShortestPathData data);
}
